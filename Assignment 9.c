#include<stdio.h>
int main()
{   //create file(assignment9.txt) and open it in write mode
    //write a sentence - UCSC is one of the leading institutes in Sri Lanka for computing studies.s
    FILE *fptr;// Declare a pointer of type file
    char file[100]= "UCSC is one of the leading institutes in Sri Lanka for computing studies.";
    fptr = fopen ("assignment9.txt" , "w"); //Open file using the fopen() function w - write
    fprintf (fptr,file);
    fclose(fptr);//Closing the file

    //open the file in read mode
    fptr= fopen ("assignment9.txt" , "r");// r- read
    char x[100];
    while (fgets(x,100,fptr))
    {
        printf("%s\n",x);
    }
    fclose(fptr);

    //open the file in append mode // append a sentence- UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.
    fptr = fopen ("assignment9.txt" ,"a"); //a- append
    fputs(" UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." , fptr);
    fclose(fptr);

    return 0;

}
